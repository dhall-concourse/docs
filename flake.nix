{
  description = "A very basic flake";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = {nixpkgs, flake-utils, ...}:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [(import ./overlay.nix)];
        };
      in rec {
        packages = {
          dev-env = pkgs.buildEnv {
            name = "dhall-concourse-docs-dev-env";
            paths = [ pkgs.dhall pkgs.dhall-nixpkgs ];
          };
          dhall-concourse-docs = pkgs.callPackage ./docs/store.nix {};
        };
        defaultPackage = packages.dhall-concourse-docs;
    });
}
