pkgsNew: pkgsOld: {
  haskellPackages = pkgsOld.haskellPackages.override(old: {
    overrides =
      let
        extension =
          haskellPackagesNew: haskellPackagesOld: {
            mmark =
              pkgsNew.haskell.lib.overrideCabal
                haskellPackagesOld.mmark
                (old: { broken = false; doCheck = false; });
            dhall-docs =
              pkgsNew.haskell.lib.overrideCabal
                haskellPackagesOld.dhall-docs
                (old: { broken = false; doCheck = false; });
          };

      in
        pkgsNew.lib.composeExtensions (old.overrides or (_: _: {})) extension;
  });
  dhallPackages = pkgsOld.dhallPackages.override(old: {
    overrides = import ./dhall-packages/overrides.nix;
  });
}
