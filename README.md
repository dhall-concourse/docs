# dhall-concourse docs

## Prerequisites

1. nix
2. direnv (optional)

## Generate

Run `nix build`

## Add packages

Add dhall packages to [dhall-packages](dhall-packages) in format `<package-name>-<version_number_without_dots>`.

1. Run command to generate basic structure:

```
dhall-to-nixpkgs github --name dhall-concourse https://github.com/<owner>/<repo> --rev <tag> --document > dhall-packages/<package-name>-<version>
```

2. Add to the package to [dhall-packages/overrides.dhall](dhall-packages/overrides.nix)

## Credits

The docs derivation is pretty much a copy of store.dhall-lang.org, copied from
[here](https://github.com/dhall-lang/dhall-lang/blob/00e67bfb7f6dd04f9c0727329211d1f8a14e6c46/nixops/store.nix).
