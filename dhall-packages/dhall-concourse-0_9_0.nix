{ buildDhallGitHubPackage, Prelude-19_0_0 }:
  buildDhallGitHubPackage {
    name = "dhall-concourse";
    githubBase = "github.com";
    owner = "akshaymankar";
    repo = "dhall-concourse";
    rev = "0.9.0";
    fetchSubmodules = false;
    sha256 = "sha256-ELIKTgdplKfOFvFwKtYY6VjLeI54Bv6iB6GM2y+TsC0=";
    directory = "";
    file = "package.dhall";
    source = false;
    document = true;
    dependencies = [ Prelude-19_0_0 ];
  }
