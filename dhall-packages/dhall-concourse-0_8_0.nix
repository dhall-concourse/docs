{ buildDhallGitHubPackage, Prelude-16_0_0 }:
  buildDhallGitHubPackage {
    name = "dhall-concourse";
    githubBase = "github.com";
    owner = "akshaymankar";
    repo = "dhall-concourse";
    rev = "0.8.0";
    fetchSubmodules = false;
    sha256 = "1fqgg1x1ki1bsv3ficssnh6h84a1lhljkckx4ws0akh3qw06n250";
    directory = "";
    file = "package.dhall";
    source = false;
    document = true;
    dependencies = [ Prelude-16_0_0 ];
  }
