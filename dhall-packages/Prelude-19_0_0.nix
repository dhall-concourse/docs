{Prelude}:
(Prelude.overridePackage {
  rev = "v19.0.0";
  sha256 = "sha256-d24FruJ3B+/neStRFCinTi9kZALJv7CzykZP2otLohI=";
})
