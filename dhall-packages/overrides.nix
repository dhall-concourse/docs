dhallPackagesOld: dhallPackagesNew: rec {
  Prelude-16_0_0 = dhallPackagesOld.callPackage ./Prelude-16_0_0.nix {};
  Prelude-19_0_0 = dhallPackagesOld.callPackage ./Prelude-19_0_0.nix {};
  dhall-concourse-0_8_0 = dhallPackagesOld.callPackage ./dhall-concourse-0_8_0.nix {};
  dhall-concourse-0_9_0 = dhallPackagesOld.callPackage ./dhall-concourse-0_9_0.nix {};
  render-optionals-0_8_0 = dhallPackagesOld.callPackage ./render-optionals.nix {
    dhall-concourse = dhall-concourse-0_8_0;
  };
  render-optionals-0_9_0 = dhallPackagesOld.callPackage ./render-optionals.nix {
    dhall-concourse = dhall-concourse-0_9_0;
  };
  git-resource-0_0_4 = dhallPackagesOld.callPackage ./git-resource-0_0_4.nix {};
}
