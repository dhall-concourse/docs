{ buildDhallGitHubPackage, Prelude-16_0_0, dhall-concourse-0_8_0, render-optionals-0_8_0 }:
  buildDhallGitHubPackage {
    name = "git-resource";
    githubBase = "github.com";
    owner = "dhall-concourse";
    repo = "git-resource";
    rev = "0.0.4";
    fetchSubmodules = false;
    sha256 = "035vbjzzfrsmbi00qr4dl4ixdsjbfwcyzbbbdfh47wbb860n7wbh";
    directory = "";
    file = "package.dhall";
    source = false;
    document = true;
    dependencies = [
      Prelude-16_0_0
      dhall-concourse-0_8_0
      render-optionals-0_8_0
    ];
  }
