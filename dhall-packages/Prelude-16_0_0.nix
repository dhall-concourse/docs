{Prelude}:
(Prelude.overridePackage {
  rev = "v16.0.0";
  sha256 = "sha256-SzdOleD17p525N58wynbOHKcTLOCxxXEEdAV1WHe19I=";
})
